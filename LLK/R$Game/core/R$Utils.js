var R$U = window["R$Utils"]={};
R$Utils.sequence = function(from,to){
	if(from>to) throw new Error("to must be greater equal than from!");
	var arr =[];
	for(var i =from;i<=to;i++){
		arr.push(i);
	}
	return arr;
};
R$Utils.randomChoice=function(arr,num){
	if(num<=0)throw new Error("num could not be less than zero");
	if(num>arr.length)throw new Error("num could not be more than the lenth of arr!");
	if(num==arr.length)return arr;
	
	var tmp =[];
	for(var i =0; i<num;i++){
		var rand = Math.floor(Math.random()*arr.length);
		tmp[i] = arr[rand];
		arr.splice(rand,1);
	}
	return tmp;
};
R$Utils.shuffle=function(arr){
	 arr.sort(function(){return Math.random()<.5?-1:1});
};
R$Utils.merge=function(a,b){
	[].push.apply(a,b);
	return a;
}