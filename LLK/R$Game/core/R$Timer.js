var R$T =window["R$Timer"] = R$Class({
	_timer:null,
	delay:0,
	last:null,
	listener:null,
	time:16, //default 16
	R$Timer:function(listener,time){
		this.listener = listener;
		if(time)this.time = time;
		this.last = +new Date;
	},
	update:function(){
		var cur = +new Date;
		this.delay += (cur - this.last);
		this.last = cur;
		if(this.delay>=this.time){
			this.listener.onTimer();
			this.delay %= this.time;
		}
	},
	start:function(){
		var self = this;
		this._timer = setInterval(function(){self.update();},1);
	},
	stop:function(){
		clearInterval(this._timer);
		this.delay =0;
	}
});