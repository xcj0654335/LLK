(function(win,doc){
	var run_flag = true;
	function Constructor(init){
		return function(){
			if(run_flag)init.apply(this,arguments);
		}
	}
	var R$Class = function(base,member){
		if(!member){
			member = base;
			base = null;
		}

		var S = member.STATIC;
		var proto = member;
		var K,F;
		if(base){
			run_flag = false;
			proto = new base;
			run_flag = true;
			for(K in member){
				proto[K] = member[K];
			}
		}

		//stipulate the first function as the constructor;
		for(K in member){
			if(typeof member[K]=="function")break;
		}

		F = Constructor(member[K]);
		proto.constructor = F;
		F.prototype = proto;
		F.toString = function(){return "Class "+K+"";};
		if(S){
			for(K in S){
				F[K] = S[K];
			}
		}
		return F;
	}

	//publish the method to window;
	win.R$Class = R$Class;
})(window,document);