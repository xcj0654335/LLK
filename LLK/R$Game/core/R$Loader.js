 /*******************************************************
 * Class Loader
 *******************************************************/
var R$L =window["R$Loader"] = function(arrSrc)
{
	var arrImg = [];
	var iLoaded = 0;
	var lisn;

	function handleLoad()
	{
		var src = arrSrc[this.tid];

		// 缓存当前图像尺寸数据
		R$Loader.imageCache[src] = {w: this.width, h: this.height};

		++iLoaded;

		// 加载进度回调
		if(typeof lisn.onProcess == "function")
			lisn.onProcess(src, iLoaded, arrSrc.length);

		// 加载完成回调
		if(iLoaded == arrSrc.length &&
		   typeof lisn.onComplete == "function")
		{
			lisn.onComplete();
		}
	}

	function handleError()
	{
		if(typeof lisn.onError == "function")
			lisn.onError(arrSrc[this.tid], iLoaded, arrSrc.length);
	}


	/**************************************************
	 * SetListener:
	 *   设置回调接口
	 * l:
	 *   process(img_src, curID, totalID){}
	 *      载入进度
	 *   error(img_src, curID, totalID){}
	 *      载入错误
	 *   complete(){}
	 *      载入完成
	 **************************************************/
	this.setListener = function(l)
	{
		if(!l)
			throw Error("Invalid interface");
		lisn = l;

		var i, oImg;
		for(var i = 0; i < arrSrc.length; ++i)
		{
			oImg = arrImg[i] = new Image();
			oImg.onload = handleLoad;
			oImg.onerror = handleError;
			oImg.tid = i;
			oImg.src = arrSrc[i];
		}
	}
};

R$Loader.imageCache = {};