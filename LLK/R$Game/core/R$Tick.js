var R$T =window["R$Tick"] =R$Class({
	_tick:null,
	_count:null,
	R$Tick:function(count){
		this._tick=this._count = count;
	},
	onTick:function(){
		if(this._tick--){
			return false;
		}else{
			this._tick=this._count;
			return true;
		}
	},
	reset:function(v){
		if(v)this._tick = this._count= v;
		else this._tick = this._count;
	}
});