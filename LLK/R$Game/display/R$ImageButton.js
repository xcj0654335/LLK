var R$ImageButton = R$Class(R$Button,{
		R$ImageButton:function(img,text,dyna){
			this.R$Button(text);
			this.border("none");
			var rect = R$Loader.imageCache[img];
			this.bgImg(img);
			if(!dyna)dyna=1;
			this.sizeTo(rect.w,rect.h/dyna);
			if(dyna!=1){
				
				this.onmouseout =function(){
					this.bgImg(img);
				};
				this.onmouseover =function(){
					this.bgImg(img,0,-this.h());
				}
			}else{
				this.bgImg(img);
				this.onmouseover =null;
				this.onmouseout =null;
			}
		}
});