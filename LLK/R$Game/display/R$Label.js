var R$CL =window["R$Label"] = R$Class(R$DynaObject,{
	R$Label:function(text){
		this.R$DynaObject();
		this.text(text);
		this.setFontSize(14);
		this.setFont("Arial");
		this.setPadding(2);
	},
	text:function(text){
		if(text)this._div.innerText = text;
		else return this._div.innerText;
	},
	setFont:function(font){
		this.css("fontFamily",font);
	},
	setFontSize:function(size){
		this.css("fontSize",size);
	},
	html:function(html){
		if(html)this._div.innerHTML = html;
		else return this._div.innerHTML;
	},
	setAlign:function(align){
		this.css("textAlign",align);
	},
	setColor:function(color){
		this.css("color",color);
	},
	setPadding:function(pad){this.css("padding",pad);}
});