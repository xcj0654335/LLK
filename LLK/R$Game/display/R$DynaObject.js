var R$D = window["R$DynaObject"] = R$Class(R$Object,{
		_removeFlag:false,
		R$DynaObject:function(){
			this.R$Object();//parent's constructor
		},
		onEnterFrame:function(){
			this.onUpdate();
			this.updateChildren();
		},
		onUpdate:function(){},
		updateChildren:function(){
			for(var layer in this._children){
				var l = this._children[layer];
					
				for(var i=0;i<l.length;i++){
					var c = l[i];
					if(!c.isRemove()){
						c.onEnterFrame();
					}else{
						l.splice(i,1);
						i--;
						this._div.removeChild(c._div);
						c.onDestroy();
						c=null;
					}
				}
			}
		},
		isRemove:function(){return this._removeFlag;},
		remove:function(){this._removeFlag=true;},
		onDestroy:function(){}
})