var R$S =window["R$Sprite"] = R$Class(R$DynaObject,{
	_alpha:100,
	R$Sprite:function(){
		this.R$DynaObject();
	},
	alpha:function(a){
		if(a||a==0){
			a= Math.min(Math.max(0,a),100);
			this.css("filter","Alpha(opacity="+(this._alpha=a)+")");
		}else return this._alpha;
	},
	fadeIn:function(v){
		if(!v)v=1;
		this.alpha(this.alpha()+v);
	},
	fadeOut:function(v){
		if(!v)v=1;
		this.alpha(this.alpha()-v);
	}
});