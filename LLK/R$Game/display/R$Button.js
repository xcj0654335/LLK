var R$CB =window["R$Button"] = R$Class(R$Label,{
	R$Button:function(text){
		this.R$Label(text);
		this.border("1px solid gray");
		this.css("cursor","pointer");
		this.bgColor("black");
		this.setColor("white");
		this.setAlign("center");
		this.onmouseover=function(){
			this.bgColor("white");
			this.setColor("black");
		};
		this.onmouseout=function(){
			this.bgColor("black");
			this.setColor("white");
		};
	}
});