var R$LS = window["R$LogoScene"] = R$Class(R$DynaObject,{
	_logo:null,
	_btn:null,
	R$LogoScene:function(){
		this.R$DynaObject();
		this.hide();
		this._logo = new R$DynaObject();
		this._logo.sizeTo(500,150);
		this._logo.bgImg("R$Game/display/logo.png");
		
		this._btn = new R$Button("Click to continue.");
		this._btn.sizeTo(200,30);
		this._btn.setPadding(5);
		this._btn.onclick=function(){
			R$App.MyApp.switchScene(R$App.MyApp._scene[0]);
		};
		this.addChild(this._btn);
		this.addChild(this._logo);
	},
	onEnter:function(){
		this.show();this._logo.x(this.w());this._logo.y(100);
		this._btn.center();
		this._btn.hide();

	},
	onLeave:function(){this.hide();},
	onUpdate:function(){
		
		if(this._logo.x()<=(this.w()-500)/2){
			this._logo.x((this.w()-500)/2);
			this._btn.show();
		}else{
			this._logo.x(this._logo.x()-5);	
			
		}
	}
});