/*
*	the root object that can be displayed on the screen;
*/
var R$O=window["R$Object"] = R$Class({
	_div:null,
	_parent:null,
	_x:0,_y:0,_w:0,_h:0,
	_v:true,
	_zIndex:-1,
	STATIC:{
		EVTYPE:["onclick",
			"onmouseout",
			"onmouseover",
			"onmousemove",
			"onmousedown",
			"onmouseup"]
	},
	R$Object:function(){
		this._div = document.createElement("div");
		this.css("position","absolute");
		this.css("overflow","hidden");
		this.css("fontSize","0px");
		this._children = {"default":[]};
		//event handler
		var self = this;
		for(var i in R$Object.EVTYPE){
			var et = R$Object.EVTYPE[i];
			(function(et){
				self._div[et] = function(){ if(self[et])self[et](event); };
			})(et);
		}
	},
	css:function(key,value){
		if(!value){
			return this._div.style[key];
		}else{
			this._div.style[key]=value;			
		}
	},
	setClass:function(name){
		this._div.className = name;
	},
	setStyle:function(style){
		for(var s in style){
			this._div.style[s]= style[s];
		}
	},
	visible:function(flag){
		if(flag||flag==false){
			this._v = flag;
			this.css("display",flag?"block":"none");
		}else{
			return this._v;
		}
	},
	show:function(){
		if(!this.visible())this.visible(true);
	},
	hide:function(){
		if(this.visible())this.visible(false);
	},
	x:function(x){
		if(x||x==0)this.css("left",(this._x = x)+"px");
		else return this._x;
	},
	y:function(y){
		if(y||y==0)this.css("top",(this._y = y)+"px");
		else return this._y;
	},
	w:function(w){
		if(w||w==0)this.css("width",(this._w = (w<=0?0:w))+"px");
		else return this._w;
	},
	h:function(h){
		if(h||h==0)this.css("height",(this._h = (h<=0?0:h))+"px");
		else return this._h;
	},
	zIndex:function(z){
		if(z)this.css("zIndex",z);
		else return this._zIndex;
	},
	sizeTo:function(w,h){
		if(w==0||h==0)throw new Error("width and height can not be set to zero!");
		this.w(w);
		this.h(h);
	},
	sizeBy:function(dw,dh){
		this.sizeTo(this.w()+dw,this.h()+dh);
	},
	moveTo:function(x,y){
		this.x(x);
		this.y(y);
	},
	moveBy:function(dx,dy){
		this.moveTo(this.x()+dx,this.y()+dy);
	},
	center:function(){
		if(this._parent){
			this.moveTo((this._parent.w()-this.w())/2,(this._parent.h()-this.h())/2);
		}else{
			this.moveTo((document.body.clientWidth-this.w())/2,(document.body.clientHeight-this.h())/2);
		}
	},
	top:function(dt){
		var d;
		d = dt||0;
		this.y(d);
	},
	left:function(dt){
		var d;
		d = dt||0;
		this.x(d)
	},
	right:function(dt){
		var d;
		d = dt||0;
		if(this._parent){
			this.x(this._parent.x()+this._parent.w()-this.w()-d);
		}else{
			this.x(document.body.clientWidth-this.w()-d);
		}
	},
	bottom:function(dt){
		var d;
		d = dt||0;
		if(this._parent){
			this.y(this._parent.y()+this._parent.h()-this.h()-d);
		}else{
			this.y(document.body.clientHeight-this.h()-d);
		}
	},
	bounds:function(){
		return{
			x:this._x,
			y:this._y,
			w:this._w,
			h:this._h
		};
	},
	border:function(str){
		this.css("border",str);
	},
	bgColor:function(color){
		this.css("backgroundColor",color);
	},
	bgImg:function(img,dx,dy,repeat){
		if(!dx)dx=0;
		if(!dy)dy=0;
		if(!repeat)repeat="no-repeat";
		this.css("background","url("+img+") "+dx+" "+dy+" "+repeat);
	},
	addChild:function(child,layer){
		if(!layer){
			this._children["default"].push(child);
		}else{
			if(!this._children[layer])this._children[layer]=[];
			this._children[layer].push(child);
		}
		child._parent = this;
		this._div.appendChild(child._div);
	},
	getChildren:function(layer){
		if(!layer)layer="default";
		return this._children[layer]?this._children[layer]:[];
	},
	removeChild:function(child){
		outer:for(var layer in this._children){
			for(var i =0; i<this._children[layer].length;i++){
				if(this._children[layer][i]==child){this._children[layer].splice(i,1);break outer;}
			}
		}
		this._div.removeChild(child._div);
		child =null;
	},
	removeAll:function(layer){
		if(layer){
			var l = this._children[layer];
			for(var i =0; i<l.length;i++){
				var child = l[i];
				l.splice(i,1);i--;
				this._div.removeChild(child._div);
				child =null;
			}
		}
		else{
			for(var layer in this._children){
					var l = this._children[layer];
					for(var i =0; i<l.length;i++){
					var child = l[i];
					l.splice(i,1);i--;
					this._div.removeChild(child._div);
					child =null;
				}
			}
		}
	},
	attach:function(ele){
		ele.appendChild(this._div);
	},
	//event handler here
	bind:function(type,handler){
		this._div["on"+type]=handler;
	}
});