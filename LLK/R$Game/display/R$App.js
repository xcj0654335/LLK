var R$A =window["R$App"]= R$Class({
	_timer:null,
	_stroke:null,
	_FPS:null,
	_title:null,
	_showLogo:null,
	_w:null,_h:null,
	_content:null,
	_outer:null,
	_inner:null,
	_hideB:null,
	_titleBar:null,
	_statusBar:null,
	_curscene:null,
	_resource:null,
	STATIC:{
		MyApp:null
	},
	R$App:function(opt){
		R$App.MyApp= this;
		this._FPS = opt.FPS||100;
		this._title = opt.title||"New App";
		this._w = opt.width||800;
		this._h = opt.height||600;
		this._stroke = opt.stroke||5;
		this._scene =opt.scene||[];
		this._showLogo =(opt.showLogo!=undefined?opt.showLogo:true);
		this._resource = opt.resource ||[];
		//define outer
		this._outer = new R$Object();
		this._outer.sizeTo(this._w+2*this._stroke+10,this._h+2*this._stroke+50);
		//this._outer.border("1px solid silver");
		this._outer.center();
		this._outer.bgColor("#AAF40F");
		//define inner
		this._inner = new R$Object();
		this._outer.addChild(this._inner);
		this._inner.sizeTo(this._w+10,this._h+50);
		//this._inner.border("3px inset black");
		this._inner.center();
		this._inner.bgColor("#444444");
		//define title bar
		this._titleBar = new R$Label(this._title);
		this._inner.addChild(this._titleBar);
		this._titleBar.sizeTo(this._w+2*this._stroke+50,22);
		this._titleBar.setColor("black");
		this._titleBar.bgColor("white");
		this._titleBar.css("borderBottom","2px solid black");
	    this._titleBar.top();

		//define status bar
		this._statusBar = new R$Label("");
		this._inner.addChild(this._statusBar);
		this._statusBar.sizeTo(this._w+2*this._stroke,25);
		this._statusBar.setColor("black");
		this._statusBar.bgColor("white");
		this._statusBar.css("borderTop","1px solid black");
	    this._statusBar.bottom();


		//define hide button
		/*this._hideB = new R$Button("Hide Frame");
		this._statusBar.addChild(this._hideB);
		this._hideB.sizeTo(100,25);
		this._hideB.onclick=function(){
			R$App.MyApp.hideFrame();
		}
		this._hideB.top();
		this._hideB.right();
		*/
		//define content 
		this._content = new R$Object();
		this._inner.addChild(this._content);
		this._content.sizeTo(this._w,this._h);
		//this._content.border("1px solid gray");
	
	    this._content.center();
		for(var i=0;i<this._scene.length;i++){
			this._scene[i].sizeTo(this._w,this._h);
			this._content.addChild(this._scene[i]);
		}

		if(this._resource.length!=0){
			new R$Loader(this._resource).setListener(this);
		}
		
		this._outer.attach(document.body);
		document.title =this._title ;
		document.body.style.background="gray";
	},
	onComplete:function(){
		if(this._showLogo){
			var logo = new R$LogoScene();
			logo.sizeTo(this._w,this._h);
			this._content.addChild(logo);
			this.switchScene(logo);
		}else{
			if(this._scene.length!=0)this.switchScene(this._scene[0]);
		}
		this._statusBar.text("Status: App load successfully!");
	},
	onProcess:function(res,cid,total){
		this._statusBar.text("Status: load resource ["+res+"] "+cid+"/"+total+"!");
	},
	onError:function(res){
		this._statusBar.text("Status: Failed to load resource ["+res+"] !");
	},
	onTimer:function(){
		if(this._curscene)this._curscene.onEnterFrame();
	},
	switchScene:function(scene){
		if(this._curscene)this._curscene.onLeave();
		scene.onEnter();
		this._curscene = scene;
	},
	shake:function(){
		for(var i =0;i<20;i++){
			window.moveBy(-10,0);
			window.moveBy(0,-10);
			window.moveBy(0,10);
			window.moveBy(10,0);
		}
	},
	setup:function(){
		if(!this._timer){
			this._timer = new R$Timer(this,1000/this._FPS);
			this._timer.start();
		}
	},
	hideFrame:function(){
		this._outer.css("background","transparent");
		this._statusBar.hide();
		this._titleBar.hide();
		this._inner.css("background","transparent");
	}
});