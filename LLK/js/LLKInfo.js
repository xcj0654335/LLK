var LLKInfo =  R$Class(R$Sprite,{
	_info:null,
	_rnum:null,
	_tnum:null,
	_rlabel:null,
	_tlabel:null,
	LLKInfo:function(){
		this.R$Sprite();
		this.sizeTo(200,100);

		this._info = new R$Label("");
		this._info.sizeTo(200,30);
		this._info.left(10);
		this._info.top(10);
		this._info.setFontSize(20);
		this.addChild(this._info);
		this._info.setColor("white");
		
		var pauseBtn = new R$ImageButton("image/pause.png");
		this.addChild(pauseBtn);
		pauseBtn.left();
		pauseBtn.bottom();
		pauseBtn.onclick = function(){
			this._parent._parent.setState(LLKScene.states.PAUSE);			
		}

		var refreshBtn = new R$ImageButton("image/refresh.png");
		this.addChild(refreshBtn);
		refreshBtn.bottom(10);
		refreshBtn.left(70);
		refreshBtn.onclick = function(){
			if(this._parent._rnum!=0){
				this._parent._parent._faceHolder.refresh();
				this._parent._rnum--;
				this._parent._rlabel.text("X"+this._parent._rnum);
			}
		}

		this._rlabel = new R$Label("");
		this.addChild(this._rlabel);
		this._rlabel.bottom(30);
		this._rlabel.left(100);
		this._rlabel.setColor("white");


		var tipBtn = new R$ImageButton("image/tip.png");
		this.addChild(tipBtn);
		tipBtn.bottom(10);
		tipBtn.left(130);
		tipBtn.onclick = function(){
			if(this._parent._tnum!=0){
				if(this._parent._parent._faceHolder.tip()){
					this._parent._tnum--;
					this._parent._tlabel.text("X"+this._parent._tnum);
				}else{R$App.MyApp.shake()}
			}
		}

		this._tlabel = new R$Label("");
		this.addChild(this._tlabel);
		this._tlabel.bottom(30);
		this._tlabel.left(160);
		this._tlabel.setColor("white");
	},
	reset:function(){
		var curLevel = this._parent._faceHolder._curLevel;
		this._info.text("Level "+curLevel+":"+levelData[curLevel-1].desc);
		this._rlabel.text("X"+levelData[curLevel-1].rnum);
		this._tlabel.text("X"+levelData[curLevel-1].tnum);
		this._rnum = levelData[curLevel-1].rnum;
		this._tnum = levelData[curLevel-1].tnum;
	}

});