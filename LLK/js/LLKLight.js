var LLKLight = R$Class(R$Sprite,{
	_c:null,
	_l:null,
	_img:null,
	_hov:null,
	flag:true,
	LLKLight:function(model,curFace,lastFace){
		this.R$Sprite();
		this.sizeTo(48,48);
		this._c = curFace;
		this._l = lastFace;
		this._hov = model.hov;
		this.tick = new R$Tick(4);
		if(model.hov=="h")this._img="image/h.png";
		else this._img="image/v.png";
		this.moveTo(model.x,model.y);
	},
	onEnterFrame:function(){
		if(this.tick.onTick())this.flag=!this.flag;
			
		if(this.flag){
			if(this._hov=="h")this.bgImg(this._img,-48,0);
			else this.bgImg(this._img,0,-48);
		}else{
			this.bgImg(this._img);
		}


		this.fadeOut();
		if(this.alpha()==95){
			this._c.remove();
			this._l.remove();
		}

		if(this.alpha()==0)this.remove();
	}
});