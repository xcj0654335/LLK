var LLKPerticle = R$Class(R$Sprite,{
	_x:null,
	_y:null,
	_t:null,
	_friction:0.91,
	_v:0,
	LLKPerticle:function(x,y,timeline){
		this.R$Sprite();
		this.sizeTo(16,16);
		this.bgImg("image/star.png");
		this.x(x);
		this.y(y);
		this._t = timeline;
		//init a force
		var f=15;
		this.force ={
			x: Math.round(Math.random()*f)*Math.cos(Math.round(Math.random()*360)),
			y: Math.round(Math.random()*f)*Math.sin(Math.round(Math.random()*360))
		};
			
	},
	onEnterFrame:function(){
		this.force.x = this.force.x * this._friction;
		this.force.y = this.force.y * this._friction;
		var dx = this._t.x()+this._t._progress.w()-this.x();
		var dy = this._t.y()+this._t._progress.h()/2-this.y();

		var v =Math.sqrt(dx*dx+dy*dy);
		var ux = dx/v;
		var uy = dy/v;
	     this._v+=.5;
		 

		if(dx<=1){
			this._parent.removeChild(this);
			this._parent._timeLine.increase(10);
		}
		
		this.x(this.x()+this.force.x+ux*this._v);
		this.y(this.y()+this.force.y+uy*this._v);

		
	}
	
});