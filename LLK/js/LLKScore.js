var LLKScore = R$Class(R$Sprite,{
	_retryBtn:null,
	_stageBtn:null,
	_nextBtn:null,
	_label:null,
	LLKScore:function(){
		this.R$Sprite();
		this.sizeTo(900,800);
		this.bgColor("");


		
		var mask = new R$Sprite();
		this.addChild(mask);
		mask.bgImg("image/score.png");
		mask.sizeTo(300,150);
		mask.center();

		this._label = new R$Label("");
		this._label.sizeTo(200,20);
		mask.addChild(this._label);
		
		this._label.y(40);
		this._stageBtn = new R$ImageButton("image/stage.png");
		
		mask.addChild(this._stageBtn);
		this._stageBtn.y(80);
		this._stageBtn.x(50);
		this._stageBtn.onclick = function(){
			this._parent._parent._parent.setState(LLKScene.states.STAGE);
		};

		this._retryBtn = new R$ImageButton("image/retry.png");
		mask.addChild(this._retryBtn);
		this._retryBtn.center();
		this._retryBtn.y(80);
		this._retryBtn.onclick = function(){
			this._parent._parent._parent.setState(LLKScene.states.INIT);
		};


		


		this._nextBtn = new R$ImageButton("image/next.png");
		mask.addChild(this._nextBtn);
		this._nextBtn.y(80);
		this._nextBtn.x(210);
		this._nextBtn.onclick = function(){
			this._parent._parent._parent._faceHolder.nextLevel();
			this._parent._parent._parent.setState(LLKScene.states.INIT);
		};
	},
	showWin:function(){
		this._label.text("YOU WIN!!!");
			this._parent.setState(LLKScene.states.SCORE);
	},
	showFail:function(){
		this._label.text("YOU Lose!!!");
		this._parent.setState(LLKScene.states.SCORE);
	}
});