var LLKStage =  R$Class(R$Sprite,{
	_levelDesc:null,
	LLKStage:function(){
		this.R$Sprite();
		this.sizeTo(300,210);

		this.build();
	},
	build:function(){
		// add the level description
		this._levelDesc = new R$Label("Please choose one stage.");
		this.addChild(this._levelDesc);
		this._levelDesc.sizeTo(300,25);
		this._levelDesc.setColor("white");
		this._levelDesc.setFontSize(16);

		//add the level selector
		for(var i =0;i<levelData.length;i++){
			var level = levelData[i];
			var l = i+1;
			var selector = new R$ImageButton("image/block.png",""+l);
			selector.setColor("#1133bb");
			selector.setFontSize(20);
			selector.sizeTo(30,30);
			selector.moveTo(10+i%8*35,30+35*parseInt(i/8));
			this.addChild(selector);
			(function(l,d){
				selector.onmouseover = function(){
					this._parent._levelDesc.text("Level "+l+":"+d);
					
				}
			})(l,level.desc);
			(function(l){
				selector.onclick = function(){
					this._parent._parent._faceHolder.setLevel(l);
					this._parent._parent.setState(LLKScene.states.INIT);
					
				}
			})(l);
		}
	}
});