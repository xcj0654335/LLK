var LLKTimeLine = R$Class(R$Sprite,{
	_progress:null,
	_total:null,
	_value:null,
	LLKTimeLine:function(){
		this.R$Sprite();
		this.sizeTo(500,20);
		this.bgImg("image/timebar.png");


		this._progress = new R$Sprite();
		this._progress.sizeTo(500,20);
		this._progress.bgImg("image/time.png");
		this.addChild(this._progress);
		
	},
	onEnterFrame:function(){
		if(this._value==0)this._parent._score.showFail();
		this._value -=1;
		this._progress.w(500*this._value/this._total);

	},
	reset:function(){
		this._total = levelData[this._parent._faceHolder._curLevel-1].time;
		this._value =this._total;
	},
	increase:function(v){
		if(this._value+v > this._total)this._value =this._total;
		else this._value += v;
	}
});