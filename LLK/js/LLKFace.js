var LLKFace = R$Class(R$Sprite,{
	_face:null,
	_rIndex:null,
	_cIndex:null,
	_size:48,
	_type:null,
	LLKFace:function(img,rowIndex,colIndex,type){
		this.R$Sprite();
		this.sizeTo(this._size,this._size);
		this.bgImg("image/face.png");

		this._rIndex = rowIndex;
		this._cIndex = colIndex;
		this._type = type;
		this._face = new R$Sprite();
		var rect = R$Loader.imageCache[img];
		this._face.sizeTo(rect.w,rect.h);
		this._face.bgImg(img);
		this.x(this._cIndex*this._size);
		this.y(this._rIndex*this._size);
		this.addChild(this._face);
		this._face.center();




		this.onclick=function(){
			var lastFace = this._parent._lastFace;
			var curFace = this;				
			if(lastFace && lastFace!=curFace){
				var  path =this._parent.check(lastFace,curFace);
				if(path){
						this._parent.line(path,curFace,lastFace);
						
						curFace = null;
				}
					lastFace.onBlur();
			}
				this.onSelect();
				this._parent._lastFace = curFace;				
		}
		this.onmouseover =function(){
			this.css("cursor","pointer");
		};
	},
	onSelect:function(){
		this.bgImg("image/face2.png");
	},
	onBlur:function(){
		this.bgImg("image/face.png");
	},
	match:function(face){
		return this._type==face._type;
	},
	onDestroy:function(){
		//TODO
		this._parent._map[this._rIndex][this._cIndex]=0;
		this._parent.reduce();
		if(!this._parent.pair())this._parent.refresh();
		for(var i =0;i<10;i++)this._parent._parent.addChild(new LLKPerticle(this._parent._x+this._x+this._w/2,this._parent._y+this._y+this._h/2,this._parent._parent._timeLine),"perticle");
	}
});