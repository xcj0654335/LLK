var LLKFaceHolder = R$Class(R$Sprite,{
	_cols:18,
	_rows:14,
	_map :null,
	_lastFace:null,
	_curLevel:1,
	_total:null,
	_skill:false,
	LLKFaceHolder:function(){
		this.R$Sprite();

		this.sizeTo(18*48,14*48);
		this.tick = new R$Tick(20);//for the special skill
		this._map =[];
		for(var i =0;i<this._rows;i++){
			this._map[i] =[];
		}
		
	},
	loadLevel:function(){
		this.reset();
		var level = levelData[this._curLevel-1];
		var iconNum = Math.ceil(level.place/level.unit);       // number of icon needed
		var data = level.data;
		var desc = level.desc;
		var imgIDs = R$Utils.randomChoice(R$Utils.sequence(1,48),iconNum);
		var choice =[];
		for(var i =0;i<level.place;i++){
			choice[i] = imgIDs[parseInt(i/level.unit)];
		}
		for(var i =0;i<level.difficulty;i++)R$Utils.shuffle(choice);     //random order

		var count = 0;
		for(var i =0;i<data.length;i++){
			var c = data.charAt(i);
			var cindex  = i% this._cols;
			var rindex = parseInt(i/this._cols);

			if(c=="1"){
				var id = choice.pop();
				this._map[rindex][cindex] = id;			//build the map
				this.addChild(new LLKFace("icon/"+id+".png",rindex,cindex,id),"group"+id);						//add the ui
			}else{
				this._map[rindex][cindex] = "0";
			}
		}
	},
	line:function(path,c,l){
		
		c.onclick=null;
		l.onclick = null;
		for(var i=0;i<path.length;i++){
			var p =path[i];
			var s = new LLKLight(p,c,l);
			this.addChild(s);
		}
	},
	check:function(a,b){ //i ->row j->col
		if(!a.match(b))return null;
		var pa = {i:a._rIndex,j:a._cIndex};
		var pb = {i:b._rIndex,j:b._cIndex};
		return this.zero(pa,pb)||this.one(pa,pb)||this.two(pa,pb);
	},
	zero:function(pa,pb){//0 corner
		var path=null;
		if(pa.i == pb.i){   //horizon check
				
				var start = Math.min(pa.j,pb.j);
				var end = Math.max(pa.j,pb.j);

				path=[{x:24+48*start,y:48*pa.i,hov:'h'}];
				for(var i = start+1;i<end;i++)if(this._map[pa.i][i]!=0){
					path=null;break;
				}else{
					path.push({x:24+48*i,y:48*pa.i,hov:'h'});
				}

		}else if(pa.j==pb.j){ //vertical check
				var start = Math.min(pa.i,pb.i);
				var end = Math.max(pa.i,pb.i);
				path=[{x:48*pa.j,y:24+48*start,hov:'v'}];
				for(var i = start+1;i<end;i++)if(this._map[i][pa.j]!=0){
					path=null;break;
				}else{
					path.push({x:48*pa.j,y:24+48*i,hov:'v'})
				}

		}else path=null;
		return path;
	},
	one:function(pa,pb){//1 corner
		/*
			pa----pd
			|      |
            |      |
			pc----pb
		*/
		var pc = {i:pb.i,j:pa.j};
		var pd = {i:pa.i,j:pb.j};
		var papc,pbpc,papd,pbpd;
		return (this._map[pc.i][pc.j]==0 && (papc=this.zero(pa,pc))&& (pbpc=this.zero(pc,pb))&& R$Utils.merge(papc,pbpc) ) || (this._map[pd.i][pd.j]==0 && (papd=this.zero(pd,pa))&& (pbpd=this.zero(pd,pb))&& R$Utils.merge(papd,pbpd) );
	},
	two:function(pa,pb){//2 corner
		var ppa,ppb;
		//left from pa find out the point that zero to pa and one to pb
		if(pa.j!=0){
			for(var n = pa.j-1;n>=0;n--){
				var p = {i:pa.i,j:n};
				if(this._map[pa.i][n]==0 && (ppa=this.zero(p,pa)) && (ppb=this.one(p,pb)))return R$Utils.merge(ppa,ppb);
			}
		}

		//up from pa find out the point that zero to pa and one to pb
		if(pa.i!=0){
			for(var n = pa.i-1;n>=0;n--){
				var p = {i:n,j:pa.j};
				if(this._map[n][pa.j]==0 && (ppa=this.zero(p,pa)) && (ppb=this.one(p,pb)))return R$Utils.merge(ppa,ppb);
			}
		}

		//rigth from pa find out the point that zero to pa and one to pb
		if(pa.j!=this._cols-1){
			for(var n = pa.j+1;n<this._cols;n++){
				var p = {i:pa.i,j:n};
				if (this._map[pa.i][n]==0 && (ppa=this.zero(p,pa)) && (ppb=this.one(p,pb)))return R$Utils.merge(ppa,ppb);
			}
		}

		//up from pa find out the point that zero to pa and one to pb
		if(pa.i!=this._rows-1){
			for(var n = pa.i+1;n<this._rows;n++){
				var p = {i:n,j:pa.j};
				if (this._map[n][pa.j]==0 && (ppa=this.zero(p,pa)) && (ppb=this.one(p,pb)))return R$Utils.merge(ppa,ppb);
			}
		}
		return null;
	},
	setLevel:function(id){
		var total = levelData.length;
		this._curLevel = Math.min(total,Math.max(0,id));
	},
	nextLevel:function(){
		if(this._curLevel==levelData.length)this.setLevel(1);
		else this.setLevel(this._curLevel+1);
	},
	refresh:function(){
		var allChild=[];
		for(var layer in this._children){
			if(layer=="default")continue;//refresh the face not the light
			var l = this._children[layer];
			R$Utils.merge(allChild,l);

			/*for(var i=0;i<l.length;i++){
				allChild.push(l[i]);
			}*/
		}
			
		for(var i =0;i<allChild.length;i++){
			var child = allChild[i];
			var rand = Math.floor(Math.random()*allChild.length);
			var another = allChild[rand];
			this.exchange(child,another);
		}

	},
	exchange:function(a,b){
		var tmpIndex = {i:a._rIndex,j:a._cIndex};
		var tmpPos = {x:a.x(),y:a.y()};

		this._map[a._rIndex][a._cIndex]= b._type;
		a._rIndex = b._rIndex;
		a._cIndex = b._cIndex;
		a.x(b.x());
		a.y(b.y());

		this._map[b._rIndex][b._cIndex]= a._type;
		b._rIndex = tmpIndex.i;
		b._cIndex = tmpIndex.j;
		b.x(tmpPos.x);
		b.y(tmpPos.y);
	},
	tip:function(){
		var o = this.pair();
		if(o){
			o.a.onSelect();
			o.b.onSelect();
			return true;
		}
		return false;
	},
	pair:function(){
		var obj=null;
		outer:for(var layer in this._children){
			if(layer=="default")continue;
			var l = this._children[layer];
			for(var i=0;i<l.length;i++){
				var one = l[i];
				for(var j=i+1;j<l.length;j++){
					var another = l[j];
					var path = this.check(one,another);
					if(path){
						obj={a:one,b:another,p:path};
						break outer;
					}
				}
			}
		}
		return obj;
	},
	reset:function(){
		this._map =[];
		for(var i =0;i<this._rows;i++){
			this._map[i] =[];
		}
		this.removeAll();
		
		this._total = levelData[this._curLevel-1].place;
		this._lastFace = null;
	},
	reduce:function(){
		this._total--;
		//to performance wellmove this to onUpdate function
		//if(this._total==0)this._parent._score.showWin();
	},
	skill:function(){
		this._skill =!this._skill;
	},
	onUpdate:function(){
		//���ɰ�ťTODO
		if(this._skill){
			if(this.tick.onTick()){
				var o = this.pair();
				if(o)this.line(o.p,o.a,o.b);
			}
		}

		if(this._total==0 && this._parent.getChildren("perticle").length==0)this._parent._score.showWin();
	}
});