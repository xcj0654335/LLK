var LLKScene = R$Class(R$Scene,{
	_startBtn:null,
	_faceHolder:null,
	_timeLine:null,
	_menu:null,//go on,resume,stage buttons.
	_stage:null,//level panel
	_score:null, //score panel
	_state:null,
	_info:null,
	STATIC:{
		states:{
			INIT:0,
			RUN:1,
			PAUSE:2,
			STAGE:3,
			SCORE:4
		}
	},
	LLKScene:function(){
		this.R$Scene();
		this.bgImg("image/bg.jpg");
	},
	onEnter:function(){
		this.show();
		this.build();R$App.MyApp.hideFrame();
		
		//this.setState(LLKScene.states.SCORE);
	},
	build:function(){
		//set the start button
		this._skill = new R$ImageButton("image/start.png","",2);
		this.addChild(this._skill);
		this._skill.top(10);
		this._skill.right(10);
		this._skill.hide();
		this._skill.onclick=function(){
			this._parent._faceHolder.skill();
		}

		//set the start button
		this._startBtn = new R$ImageButton("image/start.png","",2);
		this.addChild(this._startBtn);
		this._startBtn.center();
		this._startBtn.onclick=function(){
			this._parent.setState(LLKScene.states.INIT);
		}
		//add the menu

		this._menu = new LLKMenu();
		this.addChild(this._menu);
		this._menu.center();
		this._menu.hide();

		//add the faceholder
		this._faceHolder = new LLKFaceHolder();
		this.addChild(this._faceHolder);
		this._faceHolder.center();
		this._faceHolder.bottom(20);
		this._faceHolder.hide();
		//this._faceHolder.loadLevel(1);
		//add the timeline
		this._timeLine = new LLKTimeLine();
		this.addChild(this._timeLine);
		this._timeLine.right(70);
		this._timeLine.top(110);
		this._timeLine.hide();

		//add the stage panel
		this._stage = new LLKStage();
		this.addChild(this._stage);
		this._stage.center();
		this._stage.hide();

		//add the info panel
		this._info = new LLKInfo();
		this.addChild(this._info);
		this._info.top(40);
		this._info.left(70);
		this._info.hide();


		//add the score panel
		this._score = new LLKScore();
		this.addChild(this._score);
		this._score.center();
		this._score.hide();

	},
	setState:function(s){
		 this._state  = s;
	},
	onEnterFrame:function(){
		switch(this._state){
			case LLKScene.states.INIT:this.state_init();break;
			case LLKScene.states.RUN:this.state_run();break;
			case LLKScene.states.PAUSE:this.state_pause();break;
			case LLKScene.states.STAGE:this.state_stage();break;
			case LLKScene.states.SCORE:this.state_score();break;
		}
	},
	state_run:function(){
		this._menu.hide();
		this._faceHolder.show();
		this._timeLine.show();
		this._info.show();
		this._skill.show();
		this._timeLine.onEnterFrame();
		this._faceHolder.onEnterFrame();
		
		//perticle
		var ps = this.getChildren("perticle");
		for(var i =0;i<ps.length;i++){
			ps[i].onEnterFrame();
		}
	},
	state_init:function(){
		this._startBtn.hide();
		this._stage.hide();
		this._score.hide();
		
		this._timeLine.show();
		this._timeLine.reset();
		this._info.show();
		this._info.reset();
		this._faceHolder.show();
		this._faceHolder.loadLevel();
		this.setState(LLKScene.states.RUN);
	},
	state_pause:function(){
		this._skill.hide();
		this._menu.show();
		this._faceHolder.hide();
		this._timeLine.hide();
		this._info.hide();
		this._score.hide();
	},
	state_stage:function(){
		this._skill.hide();
		this._stage.show();
		this._score.hide();
		this._menu.hide();
		this._faceHolder.hide();
	},
	state_score:function(){
		this._skill.hide();
		this._score.show();
	}
});