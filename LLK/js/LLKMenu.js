var LLKMenu=R$Class(R$Sprite,{
	_resumeBtn:null,
	_retryBtn:null,
	_stageBtn:null,
	LLKMenu:function(){
		this.R$Sprite();
		this.sizeTo(100,280);


		this._resumeBtn = new R$ImageButton("image/resume.png");
		this._resumeBtn.x(10);
		this._resumeBtn.y(10);
		this.addChild(this._resumeBtn);
		this._resumeBtn.onclick = function(){
			this._parent._parent.setState(LLKScene.states.RUN);
		}
		this._retryBtn = new R$ImageButton("image/retry.png");
		this._retryBtn.x(10);
		this._retryBtn.y(100);
		this.addChild(this._retryBtn);
		this._retryBtn.onclick = function(){
			this._parent._parent.setState(LLKScene.states.INIT);
		}
		this._stageBtn =  new R$ImageButton("image/stage.png");
		this._stageBtn.x(10);
		this._stageBtn.y(190);
		this.addChild(this._stageBtn);
		this._stageBtn.onclick = function(){
			this._parent._parent.setState(LLKScene.states.STAGE);
		}
	}
});